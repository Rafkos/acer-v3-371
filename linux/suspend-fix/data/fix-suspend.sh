#!/bin/bash

############################################################
#                                                          #
#   This script disables the wifi connection and           #
#   bluetooth before suspending.                           #
#                                                          #
#   name: suspend-fix                                      #
#   author: Rafkos, admin@rafkos.net                       #
#   licensed under GPLv3                                   #
#                                                          #		
############################################################

# check external display
hdmiMonitor=$(cat /sys/class/drm/card0/*HDMI*/status)

# do not sleep if external display is connected
if [[ "$hdmiMonitor" = "disconnected" ]]
then
	echo "starting suspend fix..."
else
	echo "won't suspend if more than one display is connected."
	exit 0
fi

# save status of both wifi and bluetooth
wifi=$(nmcli radio wifi | grep "")
bluetooth=$(bluetooth | awk '{print $3}' | grep "")

# check if wifi is enabled, disable if true
if [[ "enabled" = "$wifi" ]]
then
	echo "disabling wifi..."
	nmcli radio wifi off
	touch /tmp/fix-suspend-wifi.e
else
	rm /tmp/fix-suspend-wifi.e
fi

# check if bluetooth is enabled, disable if true
if [ "on" == "$bluetooth" ]
then
	echo "disabling bluetooth..."
	bluetooth off
	rmmod btusb
	touch /tmp/fix-suspend-bluetooth.e
else
	rm /tmp/fix-suspend-bluetooth.e
fi

# sleep to make sure above changes have been applied
sleep 1

# suspend
su root -c "systemctl suspend"

sleep 6

############################################################
#                                                          #
#   This script reenables the wifi connection and          #
#   bluetooth after resume.                                #
#                                                          #
#   name: suspend-fix                                      #
#   author: Rafkos, admin@rafkos.net                       #
#   licensed under GPLv3                                   #
#                                                          #		
############################################################

logger "suspend resume script"

bte="/tmp/fix-suspend-bluetooth.e"
wifie="/tmp/fix-suspend-wifi.e"

sleep 3

# enable wifi if previously disabled
if [[ -f "$wifie" ]]
then
	logger "enabling wifi..."
	nmcli radio wifi on
fi

# enable bluetooth if previously disabled
if [[ -f "$bte" ]]
then
	logger "enabling bluetooth..."
	modprobe btusb
	sleep 2
	bluetooth on
fi



exit 0
