#!/bin/bash

############################################################
#                                                          #
#   Uninstaller.                                           #
#                                                          #
#   name: suspend-fix                                      #
#   author: Rafkos, admin@rafkos.net                       #
#   licensed under GPLv3                                   #
#                                                          #		
############################################################

echo "Do you wish to uninstall the suspend fix?"
echo "[y/n]"
read inst

if [[ "$inst" == "y" || "$inst" == "Y" ]]
then
	echo "Uninstallation started."
else
	echo "Uninstallation aborted by user."
	exit
fi

# checking requirements

echo "Checking requirements..."
error=false

# check if root permissions
if [[ $(/usr/bin/id -u) -ne 0 ]] 
then
    echo "- This script requires root permissions."
    error=true
fi

if [ $error = true ]
then
	echo "Uninstallation failed."
	exit
fi

# uninstallation process starting

echo "All requirements are met."
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

# remove files

printf "Removing files..."
$(rm "/etc/acpi//fix-suspend.sh")
$(rm "/etc/acpi/events/lid_close_conf")

printf "$(tput setaf 2)[ok]$(tput sgr0)\n"

# check if files removed

printf "Checking files..."
if [ -f "/etc/acpi//fix-suspend.sh" ] || [ -f "/etc/acpi/events/lid_close_conf" ]
then
	printf "$(tput setaf 1)[failed]$(tput sgr0)\n"
	echo "Unable to remove one ore more files. Please remove these files manually:"
	echo "- /etc/acpi//fix-suspend.sh"
	echo "- /etc/acpi/events/lid_close_conf"
	echo "Uninstallation failed."
	exit
else
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
fi

# check if acpid service running

printf "Restarting acpid service..."

acpid_status=$(systemctl is-enabled acpid)

if [ ! "enabled" == "$acpid_status" ]
then
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
	printf "\nService is disabled."
else
	systemctl restart acpid.service
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
fi

echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
echo "Uninstallation completed."


