#!/bin/bash

############################################################
#                                                          #
#   Possible fix for issues with some network adapters     #
#   that refuse to suspend if wifi connection is active.   #
#   This script disables the wifi connection and           #
#   bluetooth before suspending.                           #
#                                                          #
#   name: suspend-fix                                      #
#   author: Rafkos, admin@rafkos.net                       #
#   licensed under GPLv3                                   #
#                                                          #		
############################################################

echo "What is this fix for?"
echo ""
echo "This script is a possible fix for issues with some network adapters (including Atheros Qualcomm) that refuse to suspend if wifi connection is active. This script disables the wifi connection and bluetooth before suspending and reconnects them after resume."
echo ""
echo "How does it work?"
echo "The script uses acpid service to run fix-suspend.sh script on lid close when lid opens. It is important to check in /etc/acpi that there are no other scripts that try to suspend using this method."
echo ""
echo "Do you wish to install this fix? You can remove it anytime with uninstall.sh file."
echo "[y/n]"
read inst

if [[ "$inst" == "y" || "$inst" == "Y" ]]
then
	echo "Installation started."
else
	echo "Installation aborted by user."
	exit
fi

# checking requirements

echo "Checking requirements..."
error=false

# check if root permissions
if [[ $(/usr/bin/id -u) -ne 0 ]] 
then
    echo "- This script requires root permissions."
    error=true
fi

# check if acpid is installed
if ! [ -x "$(command -v acpid)" ]
then
	echo "- Please install acpid package before starting this script."
	error=true
fi

# check if nmcli is installed
if ! [ -x "$(command -v nmcli)" ]
then
	echo "- Please install a package comtaining nmcli command before starting this script."
	error=true
fi

# check if tlp is installed
if ! [ -x "$(command -v bluetooth)" ]
then
	echo "- [Warning] Please install tlp package to enable bluetooth controls. It is not required."
fi

if [ $error = true ]
then
	echo "Installation failed."
	exit
fi

# installation process starting

echo "All requirements are met."
echo "Please make sure all suspend buttons in your power manager are disabled. This is important to not interfere with lid_close acpid scripts."
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

# copy files

printf "Copying files..."
if [ ! -f "data//fix-suspend.sh" ] || [ ! -f "data/events/lid_close_conf" ]
then
	printf "$(tput setaf 1)[failed]$(tput sgr0)\n"
	echo "One ore more files missing. Please make sure data folder exists or redownload the script."
	echo "Installation failed."
	exit
else
	$(cp -r data/* /etc/acpi/)
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
fi

# check if files copied

printf "Checking files..."
if [ ! -f "/etc/acpi//fix-suspend.sh" ] || [ ! -f "/etc/acpi/events/lid_close_conf" ]
then
	printf "$(tput setaf 1)[failed]$(tput sgr0)\n"
	echo "One ore more files missing. Please make sure data folder exists or redownload the script."
	echo "Installation failed."
	exit
else
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
fi

# check if acpid service running

printf "Checking if acpid service is running..."

acpid_status=$(systemctl is-enabled acpid)

if [ ! "enabled" == "$acpid_status" ]
then
	printf "\nService is disabled, trying to enable it..."
	systemctl enable acpid.service
	acpid_status=$(systemctl is-enabled acpid)
	if [ ! "enabled" == "$acpid_status" ]
	then
		printf "$(tput setaf 1)[failed]$(tput sgr0)\n"
		echo "Unable to activate acpid service. Try to do it manually."
		echo "Installation failed."
		exit
	else
		printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
	fi
else
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
	printf "Restarting acpid service..."
	systemctl restart acpid.service
	printf "$(tput setaf 2)[ok]$(tput sgr0)\n"
fi

echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
echo "Installation completed."



